var url = require('url');
var UT = require('./tools/models/util.js');

var toolCtrls = ['auth', 'dbadmin'];

function getCtrl(name, creds) {
    if (toolCtrls.indexOf(name) > -1) {
        ret = require('./tools/ctrls/' + name + '.js');
        return typeof ret == 'function' ? ret=ret(creds) : ret;
    } else {
        if (require('fs').existsSync('./ctrls/' + name + '.js')) {
            ret = require('./ctrls/' + name + '.js');
            return typeof ret == 'function' ? ret=ret(creds) : ret;
        } else {
            return false;
        }
    }
}

var allDomains = {};
function loadDomains(cb) {
    UT.db.getDomains(function( domains ) {
        domains.map(function(d) { allDomains[d.name] = d.uid });
        UT.log.lg( allDomains, ' allDomains' );
        console.log( cb );
        typeof cb == 'function' && cb(JSON.stringify(allDomains));
    });
}

function domains(rq, cb) {
    console.log( rq,'rq');

    switch (rq.p[0]) {
        case 'list' :
            typeof cb == 'function' && cb(JSON.stringify(allDomains));
            break;
        case 'add': 
            console.log(rq.q.name);
            UT.db.queryAndCb("INSERT INTO domains (name) VALUES ($1)", [rq.q.name], function(resp) {
                console.log( ' resp from insert ');
                loadDomains( function(doms) {
                    typeof cb == 'function' && cb(doms);
                });
            });
            break;
    }
}

setTimeout(loadDomains, 500);

module.exports = {
    go: function(req, res, creds, data) {
        var rq      = UT.parseRequest(req, data);
        var ctrl    = rq.p.shift();
        var action  = 'not set';
        var srv     = false;

        ctrl == 'api' && (ctrl = rq.p.shift());
        UT.log.lg( 'CTRL:' + ctrl + '  Action:' + action);

        if (ctrl == 'admin' && creds.perms.trim() == 'REG')
            res.end(JSON.stringify({"success": false, "msg" : "Router: Admin Access Required."}) );
        else if (ctrl == 'domains') domains(rq, function(d) { res.end(d) });
        else try {
            srv     = getCtrl(ctrl, res.sessionUser);
            action  = rq.p.shift();

            data && typeof data != 'object' && (rq.q=JSON.parse(data));
            typeof rq.q == 'undefined' && (rq.q = {});
            rq.q.domain_uid = allDomains[res.host];
            typeof req.headers.authorization == 'undefined' || (rq.q.token = rq.h.authorization);

            if (srv) 
                ctrl == 'auth' ? srv[action](req, res, creds) : srv[action](res, rq.p, rq.q, data);
            else                    
                res.end('{"success": false, "msg" : "Router: controller not defined. + ' + ctrl + '"}' );

            require('fs').appendFile('./log/access.log', JSON.stringify(rq.q) + '\n');
        } catch (e) {
            res.end(JSON.stringify({"success": false, "msg" : "Router: route not defined or error occured.", "e": e}) );
            console.log( 'ROUTER catch:' , e.stack , JSON.stringify( e ), srv, action);
        }
    }
}
