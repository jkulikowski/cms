var UT = require('../tools/models/util.js');
function OUT(arg) { return JSON.stringify(arg); };

function getDomainUid() {
    return 'abc';
}

module.exports = function(creds) {
    //creds.db = 'bmr';
    //this.perms = UT.getPerms(creds.creds);
    this.mod =  new require('../models/' +  __filename.split('/').pop())(creds);

    return {
        get: function(res, p, q) {
            //q.owner_uid = res.sessionUser.uid;
            mod.get(q, function(response){ res.end(OUT(response)); })
        },
        saveBlog: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.saveBlog(q, function(response){ res.end(OUT(response)); })
        },
        reorderBlogs: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.reorderBlogs(q, function(response){ res.end(OUT(response)); })
        },
        saveItem: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.saveItem(q, function(response){ res.end(OUT(response)); })
        },
        reorderItems: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.reorderItems(q, function(response){ res.end(OUT(response)); })
        },
        delBlog: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.delBlog(q, function(response){ res.end(OUT(response)); })
        },
        delItem: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.delItem(q, function(response){ res.end(OUT(response)); })
        },
        saveCat: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.saveCat(q, function(response){ res.end(OUT(response)); })
        },
        getItem: function(res, p, q) {
            mod.getItem(q, function(response){ res.end(OUT(response)); })
        }, 
        getBlogs: function(res, p, q) {
            mod.getBlogs(q, function(response){ res.end(OUT(response)); })
        }
    }
}
