var UT = require('../tools/models/util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    //creds.db = 'bmr';
    //this.perms = UT.getPerms(creds.creds);
    this.mod =  new require('../models/' +  __filename.split('/').pop())(creds);

    return {
        save: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.save(q, function(response){ res.end(OUT(response)); })
        },
        get: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            //res.setHeader('content-type', 'text/javascript');
            res.writeHead(200, { 'Content-Type': 'application/x-javascript'});
            mod.get(q, function(response){ res.end(OUT(response)); })
        },
        getRoutes: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.getRoutes(q, function(response){ res.end(OUT(response)); })
        },
        saveRoutes: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.saveRoutes(q, function(response){ res.end(OUT(response)); })
        }
    }
}
