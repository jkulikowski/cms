var UT = require('../tools/models/util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    //creds.db = 'bmr';
    //this.perms = UT.getPerms(creds.creds);
    this.mod =  new require('../models/' +  __filename.split('/').pop())(creds);

    return {
        getDefs: function(res, p, q) {
            mod.getDefs(q, function(response){ res.end(OUT(response)); })
        },
        saveCat: function(res, p, q) {
            mod.saveCat(q, function(response){ res.end(OUT(response)); })
        },
        saveCatItem: function(res, p, q) {
            mod.saveCatItem(q, function(response){ res.end(OUT(response)); })
        },
        delCat: function(res, p, q) {
            mod.delCat(q, function(response){ res.end(OUT(response)); })
        },
        delCatItem: function(res, p, q) {
            mod.delCatItem(q, function(response){ res.end(OUT(response)); })
        },
        getStock: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.getStock(q, function(response){ res.end(OUT(response)); })
        },
        saveStock: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.saveStock(q, function(response){ res.end(OUT(response)); })
        },
        saveItem: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.saveStock(q, function(response){ res.end(OUT(response)); })
        },
        delItem: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.delItem(q, function(response){ res.end(OUT(response)); })
        },
        ppHook:function(res, p, q) {
            mod.ppHook(q, function(response){ res.end(OUT(response)); })
        }
    }
}
