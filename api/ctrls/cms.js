var UT = require('../tools/models/util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    //creds.db = 'bmr';
    //this.perms = UT.getPerms(creds.creds);
    this.mod =  new require('../models/' +  __filename.split('/').pop())(creds);

    return {
        save: function(res, p, q) {
            q.host = res.host;
            q.owner_uid = res.sessionUser.uid;
            mod.save(q, function(response){ res.end(OUT(response)); })
        },
        get: function(res, p, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.get(q, function(response){ res.end(OUT(response)); })
        },
        getRoutes: function(res, p, q) {
            mod.getRoutes(q, res);
        }
    }
}
