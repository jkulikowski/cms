var db = require('../tools/models/db.js')('lola');
var UT  = require('../tools/models/util.js');
var log = UT.log;
/*
GOOLGE client stuff
cxeV6qUXI6NSHvdKTLB6yRnj
1035165861134-v2q106kf4jgguhl10vlikmpvfq67mlh6.apps.googleusercontent.com
*/


function resp(data, success) {
    typeof data == 'undefined' && (data = []);
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}
var htmlTpl = '<a NGIF href="#ROUTE" ng-click="nav.tab=\'ROUTE\'">' +
                '\n\t\t\t\t<li ng-class="nav.tab==\'ROUTE\'|checkActive">' + 
                '\n\t\t\t\t\t<i class="fa fa-ICON"></i>' + 
                '\n\t\t\t\t\t<span class="wide">LAB</span>' + 
                '\n\t\t\t\t</li>' +
            '\n\t\t\t</a>';

var defaultRoutes = [
    { "name": "land", "menu": "false", "icon": "none", "lab": "none" },
    { "name": "settings", "menu": "true", "icon": "gears", "lab": "Settings" }
];

var routeTpl = "APP.config(['$routeProvider', 'TPLProvider', 'deviceDetectorProvider', function($routeProvider, TPLProvider, deviceDetectorProvider) {" +
    "\n\n\tvar routes = {};\n\tROUTE_NAMES" +
    ".map(function(ctrl) {\n" + 
    "\t\troutes[ctrl] = { template: TPLProvider.$get()['ctrl_' + ctrl], controller: ctrl }; \n\t});\n"; 

function getHtml(lab, name, icon, ngif) {
    return '\n\t\t\t' + htmlTpl.replace(/LAB/g, lab)
                             .replace(/ROUTE/g, name)
                             .replace(/ICON/g, icon)
                             .replace(/NGIF/g, ngif);
}

function bldRoutes( q ) {
    var routes = q.routes;

    var ctrl = ".controller('ROUTE', ['$scope', function($scope) {}])";
    var ctrlS = '';
    //var htmlS  = getHtml('', 'land', 'bars','');
    var htmlS  = '\t\t<div before-menu></div>\n\t\t\<div after-menu></div>\n\t\t<div middle-menu>';

    routes.unshift(defaultRoutes[0]);
    routes.push(defaultRoutes[1]);
    UT.log.lg( 'ROUTES', routes );

    var routeNames  = routes.map( function(r) { return r.name} );
    var routeS = routeTpl.replace(/ROUTE_NAMES/g,  JSON.stringify(routeNames));
    routeS += '\n\troutes["settings"] = { template: "<div settings></div>", controller: "settings"};\n';
    routeS += "\n\t$routeProvider\n\t\t.when('/', \t\troutes.land)\n";

    routes.map(function(r) {
        routeS += "\t\t.when('/" + r.name + "', \t\troutes." + r.name + ')\n';
        r.name != 'settings' && r.menu == 'true' && 
            (htmlS +=  getHtml(r.lab, r.name, r.icon, ''));

        ctrlS += ctrl.replace(/ROUTE/g, r.name) + '\n';
    });

    htmlS  += getHtml('Settings', 'settings', 'gears','ng-if="auth.loggedIn"');
    htmlS  += '\n\t\t</div>';
    htmlS   = '<div id="menu">\n\t<ul>\n' + htmlS + '\n\t</ul>\n</div>';

    routeS += '\t;\n}])\n' + ctrlS + ';';

    var frontPath = '../../' + q.host.split('.').shift() + '/front/';
    require('fs').writeFileSync(frontPath + 'html/menu/main.html', htmlS);
    require('fs').writeFileSync(frontPath + 'js/gen/routes.js', routeS);
}

function runMtpl(frontPath) {
    var cmd =  'cd  ' + frontPath + '/html; ls; ./mtpl';
    require('child_process').exec(cmd, function(err, resp) { console.log('EXEC',  cmd, resp ); });
}

var mod = module.exports = function(creds) {
    this.creds = creds;

    var me = null;

    return me = {
        get:   function(q, cb) { 
            var route = require('fs').readFileSync('../front/js/gen/route.js', 'utf-8');
            cb(route);
        },
        getRoutesDepr:   function(q, cb) { 
            var sql ="SELECT * FROM route WHERE owner_uid=$1 AND host=$2";
            db.query(sql,[q.owner_uid, q.host],  function(routeResp) {
                console.log( routeResp, ' route resp');
                var data = routeResp.length == 0 ? {routes: []} : routeResp[0];
                cb(resp(data, true));
            });
        },
        saveRoutes:   function(q, cb) { 
            var isUpdate  = typeof q.uid != 'undefined';

            var frontPath = '../../' + q.host.split('.').shift() + '/front/';
            require('fs').writeFile(frontPath + 'js/gen/routeConf.json', JSON.stringify(q.routes, null, 4));
            
            db.query("SELECT host,uid FROM route WHERE uid=$1 AND host=$2", [q.uid, q.host], function(checkResp) {
                var sql = isUpdate && checkResp.length > 0
                    ? "UPDATE route SET routes=$2 WHERE owner_uid=$1 AND host=$3 RETURNING uid"
                    : "INSERT INTO route (owner_uid, routes, host) VALUES ($1, $2, $3) RETURNING uid";

                var data = [q.owner_uid, JSON.stringify(q.routes), q.host];

                db.query(sql, data, function(routeResp) {
                    cb(resp(routeResp, true));
                    bldRoutes(q);
                    runMtpl(frontPath);
                }, function(err) {
                    cb(resp([err, sql, q.routes, q.owner_uid, q.host], false));
                });
            });
        }
    }
};
