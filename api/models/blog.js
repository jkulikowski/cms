var db = require('../tools/models/db.js')('cms');
var UT  = require('../tools/models/util.js');
var log = UT.log;

//var dbaa = require('../tools/models/dbaa.js');
var upd = require('../tools/models/updates.js');

function resp(data, success) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}

var mod = module.exports = function(creds) {
    this.creds = creds;

    var me = null;

    return me = {
        get:   function(q, cb) { 
            var sql =   "SELECT b.uid, b.cat_uid, cat_name, sub_cat_name, sub_sub_cat_name, cont " +
                        "FROM blog b join blog_cat c ON (b.cat_uid=c.uid) " + 
                        "WHERE owner_uid ='" + q.owner_uid + "'";
            db.q(sql, function(blogResp) {
                var ret = {};
                blogResp.map( function(b) {
                    typeof ret[b.cat_name] == 'undefined' && (ret[b.cat_name] = {});
                    typeof ret[b.cat_name][b.sub_cat_name] == 'undefined' && (ret[b.cat_name][b.sub_cat_name] = {});
                    typeof ret[b.cat_name][b.sub_cat_name][b.sub_sub_cat_name]  == 'undefined' && (ret[b.cat_name][b.sub_cat_name][b.sub_sub_cat_name] = {uid: b.cat_uid, items:[]});
                    ret[b.cat_name][b.sub_cat_name][b.sub_sub_cat_name].items.push(b);
                });
                UT.log.lg( ret );
                
                //cb(resp(blogResp, true));
                cb(resp(ret, true));
            });
        },
        genBlogs: function(q) {
            var rootPath = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front';
            me.getBlogs(q, function(r ) {
                require('fs').writeFile(rootPath + '/js/gen/blog.json', JSON.stringify(r.data, null,4), function(e, r){
                });
            });
            require('child_process').exec('cd ' + rootPath + '; bin/ver');
        },
        saveBlog: function(q, cb) {
            console.log( q.title );
            q.handle = q.title.replace(/ /g,'-');
            var upd1 = upd.gen(q, 'blog');
            UT.db.queryAndCb( upd1.sql, upd1.data, cb);
            //me.genBlogs(q);
        },
        reorderBlogs: function(q, cb) {
            var sql = q.uids.map( function(uid, i) {
                return "UPDATE blog SET seq=" + (i+1) + " WHERE uid='" + uid + "'";
            }).join(';');
            UT.db.queryAndCb(sql, [], cb);
        },
        delBlog: function(q, cb) {
            var b_item_sql = "DELETE FROM blog_item WHERE blog_uid=$1";
            var b_sql = "DELETE FROM blog WHERE uid=$1";
            var data = [q.uid];
            UT.db.queryAndCb( b_item_sql, data, function() {
                UT.db.queryAndCb( b_sql, data, function() {
                    me.getBlogs(q, cb);
                });
            });
        },
        genItems: function(q) {
            var rootPath = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front';
            me.getItem(q, function(r ) {
                require('fs').writeFile(rootPath + '/js/gen/blog/' + q.blog_uid + '.json', JSON.stringify(r.data, null,4), function(e, r){
                });
            });
            require('child_process').exec('cd ' + rootPath + '; bin/ver');
        },
        getItem: function(q, cb) {
            var sql ="SELECT * FROM blog_item WHERE blog_uid=(SELECT uid FROM blog where handle=$1) ORDER BY seq";
            UT.log.lg( 'getitem', q , sql, q.blog_uid);
            UT.db.queryAndCb(sql, [q.handle], cb);
        },
        saveItem: function(q, cb) {
            var upd1 = upd.gen(q, 'blog_item');
            //UT.log.lg( upd1 );
            UT.db.queryAndCb( upd1.sql, upd1.data, cb);
            me.genItems(q);
        },
        reorderItems: function(q, cb) {
            var sql = q.uids.map( function(uid, i) {
                return "UPDATE blog_item SET seq=" + (i+1) + " WHERE uid='" + uid + "'";
            }).join(';');
            UT.db.queryAndCb(sql, [], cb);
        },
        delItem: function(q, cb) {
            var sql = "DELETE FROM blog_item WHERE uid=$1";
            var data = [q.uid];
            UT.db.queryAndCb( sql, data, function(delItemResp) {
                UT.log.lg('delItem resp', delItemResp);
                me.getItem(q, cb);
                me.genItems(q);
            });
        },
        getBlogs: function(q, cb ) {
            var sql ="SELECT * FROM blog WHERE domain_uid='" + q.domain_uid + "' ORDER BY seq";
            UT.db.queryAndCb(sql, '', cb);
        },
        saveCat:   function(q, cb) {
            var isCatUpdate = typeof q.cat_uid != 'undefined';

            db.query("SELECT * FROM blog_cat WHERE uid=$1 AND owner_uid=$2" , [q.cat_uid, q.owner_uid], function(checkResp) {
                isCatUpdate = isCatUpdate && checkResp.length > 0;
                var sql = isCatUpdate
                    ? "UPDATE blog_cat SET cat_name=$1, sub_cat_name=$2, sub_sub_cat_name=$3 WHERE uid=$4"
                    : "INSERT INTO blog_cat (cat_name, sub_cat_name, sub_sub_cat_name, owner_uid) VALUES ($1, $2, $3, $4) RETURNING uid";

                console.log(q, checkResp, ' save cat save resp', isCatUpdate, sql);
                var refUid = isCatUpdate ? q.cat_uid : q.owner_uid;

                var data = [q.cat_name, q.sub_cat_name, q.sub_sub_cat_name, refUid];
                db.query(sql, data, function(blogResp) {
                    cb(resp(blogResp, true));
                }, function(err) {
                    cb(resp([err, sql, q.cms, q.owner_uid], false));
                });

            }, function(err) {
                console.log( err );
            }
            );
        },
        save:   function(q, cb) {
            var isCatUpdate = typeof q.cat_uid != 'undefined';
            var isUpdate    = typeof q.uid != 'undefined';

            db.query("SELECT * FROM blog_cat WHERE uid=$1 AND owner_uid=$2" , [q.cat_uid, q.owner_uid], function(checkResp) {
                isCatUpdate = isCatUpdate && checkResp > 0;
                var sql = isCatUpdate
                    ? "UPDATE blog_cat SET cat_name=$1, sub_cat_name=$2, sub_sub_cat_name=$3 WHERE uid=$4"
                    : "INSERT INTO blog_cat (cat_name, sub_cat_name, sub_sub_cat_name, owner_uid) VALUES ($1, $2, $3, $4) RETURNING uid";

                var refUid = isCatUpdate ? q.cat_uid : q.owner_uid;

                var data = [q.cat_name, q.sub_cat_name, q.sub_sub_cat_name, refUid];
                db.query(sql, data, function(blogResp) {
                    cb(resp(blogResp, true));
                }, function(err) {
                    cb(resp([err, sql, q.cms, q.owner_uid], false));
                });

            });
        }
    }
};
