var startWebsockets = false;

global.srvConfig         = require('/www/clients.js').getByDirPath(__dirname);
global.srvConfig.status  = {};
global.srvConfig.dirName = __dirname;
global.srvConfig.wwwRoot = '/www/';

setTimeout( function() {
    require('./tools/models/db.js');
    setTimeout( function() {
        require('./models/stock.js');
    }, 300);
}, 100);

var url         = require('url'), 
    formidable  = require('formidable'),
    auth        = require('./tools/ctrls/auth.js'),
    util        = require('./tools/models/util.js'),
    rt          = require('./route.js');

var publicRoutes = require('./public.json');

require('./tools/models/dbaa.js');

var srv = require('http').createServer(function (request, response) {
  	request.url == '/favicon.ico' || auth.checkCookie(request, function(user) {
        response.sessionUser = user;
        response.host = request.headers.host;
        user && typeof global.srvConfig.status[user.uid] == 'undefined' && (global.srvConfig.status[user.uid] = 0);

        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
        response.setHeader("Content-Type", "application/json; charset=utf-8");

        var path = url.parse(request.url, true).path;
        var fileUpload = path.indexOf('files/uploadFile') != -1;

        //fileUpload = false; // HARD
        var rq     = require('./tools/models/util.js').parseRequest(request, data);
        var cmsGet = rq.p[0] == 'cms' && rq.p[1] == 'get'; // bypass
        var fbGet  = rq.p[0] == 'fb' && rq.p[1] == 'getMsg'; // bypass 
        if (fileUpload) {
            new formidable.IncomingForm().parse(request, function(err, fields, files) {
                response.writeHead(200, {'content-type': 'application/json'});
                rt.go(request, response, user, fields);
            });
        } else {
            if (user || cmsGet || fbGet) {
                if (request.method == 'POST') {
                    var body = '';
                    request.on('data',function(data) { body += data; });
                    request.on('end', function(data) { 
                                                       rt.go(request, response, user, body); });
                } else                               { rt.go(request, response, user, false); }

                path.indexOf('validLogin') == -1
                    && auth.updCookie(request, request.headers['sess-cookie']);
            } else {
                if (publicRoutes.indexOf(rq.p.join('/')) > -1) {
                    user = { ip: '173.179.242.39', domain: 'cms.jktools.pro', cookie: 'eec73b78caa3ceb2fd71bcd3b025d5aa', creds: 'REG:frame',
                        login: 'general_reg', uid: '07955c31-543a-c09d-146e-7a746d1da872' };
                    rt.go(request, response, user, false);
                }  else {
                    path.indexOf('logout') > -1 // this should never happen  TODO
                        ? auth.logout(request, response)
                        : auth.login(request, response);
                }

            }
        }

        require('fs').appendFile('./log/access.log',  request.method + ': ' +
            new Date().toUTCString() + 
            ' [' + request.headers['x-real-ip'].trim() + '] :' +
            url.parse(request.url, true).pathname + ':\n'
        );
	});
}).listen(global.srvConfig.srvPort );

console.log('API Server running at http://127.0.0.1:/' + global.srvConfig.srvPort );

if (startWebsockets) {
    var m = null;
    var cons= {};
    var wss = require('sockjs').createServer();

    function data(message, connId) { 
        for (var cId in cons) 
            cId == connId || cons[cId].write(message);
    };

    wss.on('connection', function(conn) {
        console.log( ' CONNECTED ON: ' , conn.id);
        conn.on('close', function() { 
            for (var cId in cons)
                if (cId == conn.id) 
                    delete cons[cId];
        });
        conn.on('data', function(msg) { 
            if (msg == '') {
                setTimeout( function() {
                    conn.write(JSON.stringify(["from socket"]));
                }, 20);
            }
            else {
                m = msg;
                data(m, conn.id); } 
            }
        );

        cons[conn.id]  = conn;
        setInterval( function() {
                    conn.write(JSON.stringify(["from socket Interval"]));
        }, 3000);
    });

    wss.installHandlers(srv, {prefix: '/data'});
}

process.on('uncaughtException', function (err) {
    console.log('UNCAUGHT', err.stack);
    require('fs').appendFile('./log/uncaught.log', 
        '------------ BMR API uncaught' + (new Date()).toUTCString() + ' -------------\n' + 
        err.stack + '\n'
    );
}); 
