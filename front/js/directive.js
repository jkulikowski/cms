APP
.directive('ctrlSland', ['TPL', 'api', 'fbSvc', function(TPL, api, fbSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.dctrl_sland,
        link: function($scope, el, attrs) {

            //window.fbAsyncInit = function() {

            $scope.msgs = [{user: 'Black Tie Support', msg: {
                id: '3823940243184', 
                items:  [
                            {msg: 'How do you get to your shop from Hwy 99',    from:false, ts: new Date().toISOString().substring(11,19)},
                            {msg: 'Turn left right after Safeway', from:true,  ts: new Date().toISOString().substring(11,19)},
                            {msg: 'Do you have my equipment ready',   from:false, ts: new Date().toISOString().substring(11,19)},
                            {msg: 'yup, it is waiting for you', from:true,  ts: new Date().toISOString().substring(11,19)}
                        ]
            }}];

            $scope.send = function() {
                fbSvc.send($scope.lastMsg, function(resp) {
                    console.info( resp, ' resp from send');
                });
            }

            $scope.get = function() {
                console.log( $scope.lastMsg );
                fbSvc.get(function(msgs) {
                    console.log( msgs, ' msgs for user');
                });
            }
        }
    }
}])
.directive('backImg', [function() {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, el, attrs) {
            $scope.back1 = './files/bb0503c3-011c-ac3f-8332-bef5ac0faab3/main/Desktop-Milton-min.jpg';
            $scope.back2 = './files/bb0503c3-011c-ac3f-8332-bef5ac0faab3/main/Desktop-Parc-min.jpg';
            $scope.back3 = './files/bb0503c3-011c-ac3f-8332-bef5ac0faab3/main/Desktop-William-min.jpg';
        }
    }
}])
.directive('lax', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, el, attrs) {
            window.addEventListener('scroll', function(e) {
               var scroll = window.pageYOffset;
               el.css("transform","translateY(" + (1*scroll)/2 + "px)");
            });
        }
    }
}])
.directive('beforeMenu', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.menu_before,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('afterMenu', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.menu_after,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('staffDrop', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        //template: TPL.menu_after,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('contElMenu', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        //template: TPL.menu_after,
        link: function($scope, el, attrs) {
            var contEl = angular.element(el[0].parentNode)[0];
            $scope.opts = {
                containment: 'div'
            }
        }
    }
}])
/*
.directive('product', ['TPL', 'FTPL', 'dragSvc', function(TPL, FTPL, dragSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.product,
        link: function($scope, el, attrs) {
            INFO( ' prod ');
            $scope.setProdIdx = function(idx, loc) { 
                $scope.prodIdx = idx; 
                $scope.locName = loc; 
            }

            $scope.hasImg = false;
            $scope.nav = { catIdx : '' }; 

            $scope.prodIdx = 0;
            $scope.locName = { lab: 'Lola Main'}; //Hack for now
            $scope.nav.catIdx = 0;

            $scope.addItem = function(catIdx) {
                var cat = $scope.cms.locs[$scope.prodIdx].cat[$scope.nav.catIdx];
                cat.items.push({name:'', price:'', descr:'', hippie: ''});

                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, FTPL.cms_menuItem_pop);
            }

            $scope.addCat = function(idx) {
                if (typeof idx == 'undefined') {
                    notifSvc.warn('Please select location', 3); 
                } else {
                    console.log( idx, ' prod idx', $scope.cms.locs[idx] );

                    if (typeof $scope.cms.locs[idx].cat == 'undefined')
                        $scope.cms.locs[idx].cat = [];

                    $scope.cms.locs[idx].cat.push({lab:'', items:[]});

                    console.log( $scope.cms.locs[idx].cat );
                }
            }
                
            $scope.saveOrder = function() {
                console.info('saveOrder');
            }

            $scope.reorder = function(list) {
                $scope.orderSubList = list;
                var reorderTpl = '<div ng-if="reorder" reorder order-list="orderSubList" handle="name" save-order="saveOrder()"></div>';
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, reorderTpl, 'topRight');
            }

            $scope.pencilClick = $scope.reorder;
        }
    }
}])
*/
.factory('fileSvc', ['api', function(api) {
    var files = null;
    var cbs = [];

    function loadFiles(cb) {
        api.get('files/getDirs', function(resp) {
            files = resp.data;
            files.all.map( function(f) {
                f.path = 'files/' + f.owner_uid + '/' + f.grp + '/' + f.name;
            });

            for (var i in files.grp) {
                files.grp[i].map(function(f) {
                    f.path = 'files/' + f.owner_uid + '/' + i + '/' + f.name;
                });
            }

            cbs.map(function(cb) {
                typeof cb == 'function' && cb(files);
            });
            typeof cb == 'function' && cb(files);
        });
    }
    return { 
        loadFiles: loadFiles,
        getFiles: function() { return files; },
        reg: function(cb) { 
            files == null ?  cbs.push(cb) : cb(files); 
        }
    };
}])
.directive('files', ['TPL', 'notifSvc', 'fileSvc', 'api', function(TPL, notifSvc, fileSvc, api) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.files,
        link: function($scope, el, attrs) {
            $scope.groupFiles = true;

            function findInList(file) {
                $scope.imgList.filter( function(m) {
                    console.log( 'ZZZ', file, m);
                });
            }

            $scope.removeChoice = function(file) {
                $scope.imgList = $scope.imgList.filter(function(f) {
                    return f.file != file.file;
                });
            }

            $scope.chooseFile = function(file, $event) {
                if (typeof $scope.imgList == 'undefined') {
                    $scope.setPath( file , $scope.nav.selGrp, $scope.owner_uid); 
                } else {
                    if (typeof $scope.imgList[file.uid] == 'undefined') {
                        $scope.imgList.push({file: file.path});
                        typeof $scope.setPath == 'undefined' || $scope.setPath( file );
                        console.info( $scope.imgList , ' img list' );
                    } else {
                        delete $scope.imgList[file.uid];
                    }
                }
                console.info( $scope.imgList, ' img list ' );
            }

            $scope.createGrp = function() {
                if ($scope.nav.newGrp != 'undefined' && $scope.nav.newGrp) {
                    $scope.files[$scope.nav.newGrp] = [];
                    $scope.nav.selGrp = $scope.nav.newGrp;
                }
                $scope.nav.addGrp = false;
            }

            $scope.upDone = function() { 
                fileSvc.loadFiles(function(files) {
                    console.info( ' after up file ', files, $scope.nav.selGrp );
                    $scope.files = files.grp;
                    //setTimeout(function() { $scope.$$phase || $scope.$digest(); }, 40);
                }); 
            }

            $scope.nav = {};

            function loadedFiles(files) {
                console.log( files, ' loaded files ' );
                $scope.owner_uid = files.owner_uid;

                $scope.files = $scope.groupFiles ? files.grp : files.all;
                $scope.groupFiles && 
                    ($scope.nav.selGrp = Object.keys($scope.files)[0]);
            }

            fileSvc.reg(loadedFiles);

            $scope.delImg = function(uid) {
                notifSvc.warn('This image will be deleted. Please confirm', 5, function(yN) {
                    yN && api.get('files/delFile?uid=' + uid, function(resp) {
                        if (resp.success) { fileSvc.loadFiles(function(files) {
                            console.info( '>>> after del fiels', files );
                            $scope.files = files.grp;
                        }); }
                    });
                })
            }
        }
    }
}])
.directive('footer', ['TPL', 'notifSvc', function(TPL, notifSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.footer,
        link: function($scope, el, attrs) {

            $scope.delLink = function(idxA) {
                console.log( 'footer delLink', idxA, 'abc');
                notifSvc.warn('Remove Link, Please confirm', 10, function(yes) {
                    if (yes) 
                        $scope.cms.links[idxA[0]].splice(idxA[1],1);
                    else
                        notifSvc.info('Remove Link aborted ', 3);
                });
            }
        }
    }
}])
.filter('adjustWord', function() {
    return function(input) {
        var ret;
        switch(input) {
            case 'mo':         ret = "Monday";       break;
            case 'tu':         ret = "Tuesday";      break;
            case 'we':         ret = "Wednesday";    break;
            case 'th':         ret = "Thursday";     break;
            case 'fr':         ret = "Friday";       break;
            case 'sa':         ret = "Saturday";     break;
            case 'su':         ret = "Sunday";       break;
            default :          ret = input;          break;
        }
        return ret;
    };
})
;
