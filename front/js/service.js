APP
.factory('fbSvc', ['api', function(api) {
    var myUserId = null;
    var fbInterval = null;

    function FB_init() {
        console.log( typeof FB, ' typeof fb ');
        if (typeof FB != 'undefined') {
            FB.init({
                appId      : '166755083457206',
                xfbml      : true,
                version    : 'v2.3'
            });

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    console.log('Logged into facebook.', response.authResponse);
                    myUserId = response.authResponse.userID;
                } else {
                    console.log( ' need to login to Facebook');
                    FB.login();
                }
                console.info( myUserId, 'my user id' );
            });

            console.info( ' clear ing ' );
            clearInterval(fbInterval);
            fbInterval = null;
        }
    }

    function send(msg) {
        api.get('fb/sendToPage', 'msg=' + $scope.lastMsg + '&id=' + myUserId, function(resp) {
            console.log( resp, ' sent message' );
        });
    }

    function get() {
        console.log( $scope.lastMsg );
        api.get('fb/getMsgs', 'id=' + myUserId, function(resp) {
            console.log( resp, ' sent message' );
        });
    }

    fbInterval = setInterval(FB_init, 100);

    return {
        get:    get,
        send:   send 
    }
}])
